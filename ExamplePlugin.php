<?php

/**
 * Example Stud.IP plugin.
 */
class ExamplePlugin extends StudIPPlugin implements SystemPlugin
{
    public function __construct()
    {
        parent::__construct();

        require_once 'vendor/autoload.php';
    }

    public function perform($unconsumedPath)
    {
    }
}
